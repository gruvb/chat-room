{application,benchwarmer,
             [{applications,[kernel,stdlib,elixir]},
              {description,"Benchwarmer is an Elixir micro-benchmarking utility that runs a function (or\nlist of functions) repeatedly against a dataset for a period of time, and\nthen reports on the average time each operation took to complete, allowing\nfor easy comparison.\n"},
              {modules,['Elixir.Benchwarmer','Elixir.Benchwarmer.Results',
                        'Elixir.Benchwarmer.Results.Helpers',
                        'Elixir.String.Chars.Benchwarmer.Results']},
              {registered,[]},
              {vsn,"0.0.2"}]}.
