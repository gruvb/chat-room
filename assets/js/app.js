// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"


function ping(host, port, pong) {

    var milliseconds;
    var started = new Date().getTime();
  
    var http = new XMLHttpRequest();
  
    http.open("GET", "http://" + host + ":" + port, /*async*/true);
    http.onreadystatechange = function() {

      if (http.readyState == 4) {
        var ended = new Date().getTime();
  
        var milliseconds = ended - started;
  
        if (pong != null) {
          pong(milliseconds);
        }
      }
    };
    try {
      http.send(null);
    } catch(exception) {
      // this is expected
    }
    return milliseconds;
  }

  ping("localhost", "4000", function(m){ console.log("It took "+m+" miliseconds.");})

  console.log("Working File");

// Import local files
//
// Local files can be imported directly using relative paths, for example:
import socket from "./socket"

// module.exports = app;